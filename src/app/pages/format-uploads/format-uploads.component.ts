import {Component, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';
import {IItem} from '../../services/invoice/ItemInterface';
import {NotifierComponent} from '../../components/notifier/notifier.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {ProviderService} from '../../services/provider/provider.service';
import {AdcService} from '../../services/adc/adc.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {IProvider} from '../../services/provider/ProviderInterface';
import {ICompany} from '../../services/adc/companyInterface';
import {InvoicesService} from '../../services/invoice/invoices.service';
import {IProviderInvoice} from '../../services/invoice/ProviderInvoiceInterface';
import {ICompanyInvoice} from '../../services/invoice/CompanyInvoiceInterface';
import {MatProgressBarModule} from '@angular/material/progress-bar';

@Component({
  selector: 'app-format-uploads',
  templateUrl: './format-uploads.component.html',
})
export class FormatUploadsComponent implements OnInit {
  doClean = false;
  data: any = {
    product: null,
    selectedStartDate: null,
    selectedEndDate: null,
    invoice_number: null,
    operation_number: null,
  };
  uploadingFirst = false;
  uploadingSecond = false;
  percentageFirst = 0;
  percentageSecond = 0;
 
  @Output() Clean = new EventEmitter<boolean>();

  // tslint:disable-next-line:max-line-length
  constructor(private invoicesService: InvoicesService, public fb: FormBuilder, private snackBar: MatSnackBar) {
    
  }

  ngOnInit(): void {

  }

 



  open_file(): void {
    const element = document.getElementById('file_input');
    console.log('file_input', element);
    if (element) {
      element.click();
    }
  }

  uploadPDF(): void {
    this.uploadingFirst = true;
    const count = setInterval(() => {
      if (this.percentageFirst < 100) {
        this.percentageFirst++;
      } else {
        clearInterval(count);
        this.percentageFirst = 0;
        this.uploadingFirst = false;
        this.snackBar.openFromComponent(NotifierComponent, {
          data: {
            message: 'Se ha subido correctamente el archivo',
            dismiss: 'Cerrar',
            type: 'Aviso'
          },
          duration: 1500,
          panelClass: 'alert-success'
        });
      }
    }, 50);

  }
  uploadFile(): void {
    this.uploadingSecond = true;
    const count = setInterval(() => {
      if (this.percentageSecond < 100) {
        this.percentageSecond++;
      } else {
        clearInterval(count);
        this.percentageSecond = 0;
        this.uploadingSecond = false;
        this.snackBar.openFromComponent(NotifierComponent, {
          data: {
            message: 'Se ha subido correctamente el archivo',
            dismiss: 'Cerrar',
            type: 'Aviso'
          },
          duration: 1500,
          panelClass: 'alert-success'
        });
      }
    }, 50);

  }
  downloadPDF(): void {
    this.uploadingFirst = true;
    const count = setInterval(() => {
      if (this.percentageFirst < 100) {
        this.percentageFirst++;
      } else {
        clearInterval(count);
        this.percentageFirst = 0;
        this.uploadingFirst = false;
        this.snackBar.openFromComponent(NotifierComponent, {
          data: {
            message: 'Se ha descargado correctamente el archivo',
            dismiss: 'Cerrar',
            type: 'Aviso'
          },
          duration: 1500,
          panelClass: 'alert-success'
        });
      }
    }, 50);

  }
  downloadFile(): void {
    this.uploadingSecond = true;
    const count = setInterval(() => {
      if (this.percentageSecond < 100) {
        this.percentageSecond++;
      } else {
        clearInterval(count);
        this.percentageSecond = 0;
        this.uploadingSecond = false;
        this.snackBar.openFromComponent(NotifierComponent, {
          data: {
            message: 'Se ha descargado correctamente el archivo',
            dismiss: 'Cerrar',
            type: 'Aviso'
          },
          duration: 1500,
          panelClass: 'alert-success'
        });
      }
    }, 50);

  }


  /** Whether the number of selected elements matches the total number of rows. */
  /*  isAllSelected(): boolean {
      const numSelected = this.selection.selected.length;
      const numRows = this.invoices.length;
      return numSelected === numRows;
    }

    /!** Selects all rows if they are not all selected; otherwise clear selection. *!/
    // tslint:disable-next-line:typedef
    masterToggle() {
      if (this.isAllSelected()) {
        this.selection.clear();
        return;
      }
      this.selection.select(...this.invoices);
    }

    /!** The label for the checkbox on the passed row *!/
    checkboxLabel(row?: IItem): string {
      if (!row) {
        return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
      }
      return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row `;
    }*/
}
